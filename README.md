# Recommendation Microservice

## Overview

### Query recommendations
This is an example microservice, that offers recommendation requests for given customers. You can access the service via endpoint with GET request "/customers/111111/games/recommendations?count=5" where 111111 is the customerNumber to be replaced with your customer. The count parameter sets maximum of resulting recommendations.
 
The results are produced in JSON format: {"customerId":111111,"recommendations":["sevenwins","brilliant"]}. Here the customerId will be same as in used request. The JSON-Array "recommendations" contains the list of games to be  presented.

There will be a NotFound-error (404), if customer not found, or customer inactive (see RECOMMENDATION_ACTIVE flag in dataset).
Otherwise status will be OK (200) with JSON in response body.
  
### Upload dataset  
To upload your customer and recommendation data, use POST request on "/upload" with parameter "file" pointing to a MultipartFile. The file must have CSV-extension. Fileformat has to be "CUSTOMER_NUMBER","RECOMMENDATION_ACTIVE","REC1","REC2","REC3","REC4","REC5","REC6","REC7","REC8","REC9", "REC10", where CUSTOMER_NUMBER must be numberic, and RECOMMENDATION_ACTIVE must be "TRUE" or "true" to activate recommendations.

On error case, a errorCode >= 400 will be served with detailed error message in response body element. Otherwise statusCode OK (200) will be send.

## Build project with maven
Simple build your application with "maven install". All dependencies will be resolved and a big runnable jar will be created in your target folder. 

Afterwards just run the big Über-jar via "java -jar %file%". In your console some Spring information will be shown  and an application will start, normally on port 8080.

Visit the URL "http://127.0.0.1:8080". A simple web front end will be loaded where you can test the endpoints.

## Run in IDE
Just start the main-method in de.esailors.gamerecommendation.Application. Spring boot will start.

## Build with Docker
Make sure, that environment variable "DOCKER_HOST" is configured. 

Run "mvn install dockerfile:build" to create a docker image on your host. To modify the repository, use "-Ddocker.image.prefix=repo" in command and change "repo" to your repository name. 
On same machine as you build the docker image, you can run it via: "docker run -p 8080:8080 -t beberhardt/gamerecommendation". This will start your created docker image and expose the port 8080 to your hosts machine, so that it is accessable.

Visit the URL "http://DOCKER_HOST:8080", where DOCKER_HOST is the Host you run your docker application. A simple web front end will be loaded where you can test the endpoints.


## Use real, persistent database
To configure a persistent database, for example mysql, just edit "application.properties" and add the following lines to this file:

* spring.datasource.url = jdbc:mysql://mysql:3306/db
* spring.datasource.username = usr
* spring.datasource.password = root
* spring.jpa.hibernate.ddl-auto = create
* spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect


Then start a mysql-container via:
* docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=db -e MYSQL_USER=usr -e MYSQL_PASSWORD=root -d mysql:5.6

Verify via "docker ps" whether the container has started without errors.

Now run our recommendation service as described above but add "--link mysql:mysql" so that the configured datasource can be used from other docker container. 



