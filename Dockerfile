FROM openjdk:8-jdk-alpine
MAINTAINER bj.eberhardt@web.de
VOLUME /tmp
# jboss will use this directory, so create it as volume is much easier
ARG JAR_FILE
# argument will be used in maven build process. Filename to the runnable jar must be set.
ADD ${JAR_FILE} application.jar
# will be executed on docker-run
ENTRYPOINT ["java","-jar","/application.jar"]