package de.esailors.gamerecommendation.repositories;

import de.esailors.gamerecommendation.model.persistence.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Simple CRUD repository for handling Customer entity and its child entities from persistence layer
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
