package de.esailors.gamerecommendation.model.view;

import java.util.List;

/**
 * Model for view of REST service which is processed and marshalled later in JSON
 */
public class RecommendationDTO {

  private long customerId;
  private List<String> recommendations;

  /**
   * New instance with given data
   *
   * @param pCustomerId      customer number
   * @param pRecommendations list of recommendations
   */
  public RecommendationDTO(long pCustomerId, List<String> pRecommendations) {
    this.customerId = pCustomerId;
    this.recommendations = pRecommendations;
  }

  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(long pCustomerId) {
    this.customerId = pCustomerId;
  }

  public List<String> getRecommendations() {
    return recommendations;
  }

  public void setRecommendations(List<String> pRecommendations) {
    this.recommendations = pRecommendations;
  }

  @Override public boolean equals(Object pObject) {
    if (this == pObject)
      return true;
    if (pObject == null || getClass() != pObject.getClass())
      return false;

    RecommendationDTO that = (RecommendationDTO) pObject;

    return customerId == that.customerId;
  }

  @Override public int hashCode() {
    return (int) (customerId ^ (customerId >>> 32));
  }

  @Override public String toString() {
    return "RecommendationDTO{customerId=" + customerId + '}';
  }
}
