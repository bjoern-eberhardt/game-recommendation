package de.esailors.gamerecommendation.model.persistence;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Persistence model of Customer entity, which has attributes and a list of recommendations
 * ({@link RecommendationEntry}.
 */
@Entity
@Table(name = "CUSTOMER")
public class Customer {

  @Id
  @Column(name = "ID", unique = true, nullable = false)
  private Long customerNumber;

  @Column(name = "RECOMMENDATION_ACTIVE")
  private boolean recommendationActive;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "CUSTOMER_ID")
  private List<RecommendationEntry> recommendationEntries;

  protected Customer() {
    recommendationEntries = new ArrayList<>();
  }

  public Customer(Long pCustomerNumber) {
    this();
    customerNumber = pCustomerNumber;
  }

  public Long getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(Long pCustomerNumber) {
    this.customerNumber = pCustomerNumber;
  }

  public boolean isRecommendationActive() {
    return recommendationActive;
  }

  public void setRecommendationActive(boolean pRecommendationActive) {
    this.recommendationActive = pRecommendationActive;
  }

  public List<RecommendationEntry> getRecommendationEntries() {
    return recommendationEntries;
  }

  public void setRecommendationEntries(
      List<RecommendationEntry> pRecommendationEntries) {
    this.recommendationEntries = pRecommendationEntries;
  }

  @Override
  public String toString() {
    return "Customer-" + customerNumber;
  }

  /**
   * Simple method to add recommendations and link child objects with its parent entitiy.
   *
   * @param pEntries List of recommendations to be linked with actual customer entity
   */
  public void addRecommendations(RecommendationEntry... pEntries) {
    for (RecommendationEntry entry : pEntries) {
      if (entry != null) {
        entry.setCustomer(this);
        getRecommendationEntries().add(entry);
      }
    }
  }

}
