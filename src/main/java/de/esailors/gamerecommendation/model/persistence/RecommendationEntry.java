package de.esailors.gamerecommendation.model.persistence;

import javax.persistence.*;

/**
 * Persistence model of Recommendations table containing all recommendation entries
 */
@Entity
@Table(name = "RECOMMENDATIONS")
public class RecommendationEntry {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "RECOMMENDATION_ID", unique = true)
  private Integer id;

  @ManyToOne
  private Customer customer;

  @Column(name = "RECOMMENDATION")
  private String recommendation;

  public RecommendationEntry() {

  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public RecommendationEntry(String pRecommendation) {
    recommendation = pRecommendation;
  }

  public String getRecommendation() {
    return recommendation;
  }

  public void setRecommendation(String pRecommendation) {
    this.recommendation = pRecommendation;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer pCustomer) {
    this.customer = pCustomer;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(recommendation)
           .append(" (")
           .append(customer.toString())
           .append(")");
    return builder.toString();
  }

}
