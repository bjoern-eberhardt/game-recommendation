package de.esailors.gamerecommendation.tools;

import java.util.List;

/**
 * Representation PPOJO of a single CSV line of RecommendationUpload-CSV-format.
 */
public class RecommendationUploadCSVRowData {
  private long customerNumber;
  private boolean recommendationsActive;
  private List<String> recommendations;

  /**
   * New instance with given details
   *
   * @param pCustomerNumber        customerNumber
   * @param pRecommendationsActive should recommendations be active for this customer?
   * @param pRecommendations       list of products used for recommendations
   */
  public RecommendationUploadCSVRowData(long pCustomerNumber, boolean pRecommendationsActive, List<String>
      pRecommendations) {
    this.customerNumber = pCustomerNumber;
    this.recommendationsActive = pRecommendationsActive;
    this.recommendations = pRecommendations;
  }

  /**
   * Returns the customer number
   *
   * @return customer number
   */
  public long getCustomerNumber() {
    return customerNumber;
  }

  /**
   * Returns whether recommendations are active for this customer
   *
   * @return true, if recommendations are active
   */
  public boolean isRecommendationsActive() {
    return recommendationsActive;
  }

  /**
   * Get a list of recommendations used for this customer
   *
   * @return modificable list of recommendations for this customer
   */
  public List<String> getRecommendations() {
    return recommendations;
  }
}
