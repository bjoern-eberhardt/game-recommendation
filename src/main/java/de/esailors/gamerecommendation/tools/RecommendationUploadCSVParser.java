package de.esailors.gamerecommendation.tools;

import de.esailors.gamerecommendation.service.exceptions.RecommendationUploadServiceException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser for CSV Inputstream data in RecommendationUpload format:
 * <br>
 * <code>"CUSTOMER_NUMBER","RECOMMENDATION_ACTIVE","REC1","REC2","REC3",
 * REC4","REC5","REC6","REC7","REC8","REC9","REC10"</code> <br>
 * <ul>
 * <li>Header should be included into data.</li>
 * <li>CUSTOMER_NUMBER cell must be a number.</li>
 * <li>RECOMMENDATION_ACTIVE must be of "true"
 * or "TRUE" to be set as active. </li>
 * <li>REC1 until REC10 must be single string interpreted as product name.</li>
 * </ul>
 */
public class RecommendationUploadCSVParser {

  /**
   * New parser instance.
   */
  public RecommendationUploadCSVParser() {
  }

  /**
   * Parses the input stream which is expected to be CSV data with Header information
   *
   * @param pInputStream Input data
   * @return parsed CSV Line in POJO
   * @throws RecommendationUploadServiceException error occured during parsing
   */
  public List<RecommendationUploadCSVRowData> parse(InputStream pInputStream)
      throws RecommendationUploadServiceException {
    List<RecommendationUploadCSVRowData> entries = new ArrayList<>();
    try {
      //read out each CSV line, try to parse data and check if correct format (customer = number)
      for (CSVRecord record : CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(new InputStreamReader(pInputStream))) {
        entries.add(parseCSVRow(record));
      }
    } catch (NumberFormatException e) {
      throw new RecommendationUploadServiceException("Error parsing data. Uncorrect format of customer-number.");
    } catch (IOException e) {
      throw new RecommendationUploadServiceException("Error parsing stream data. I/O Error. ");
    } catch (Exception e) {
      throw new RecommendationUploadServiceException("Error parsing data. Uncorrect format: " + e.getMessage());
    }
    return entries;
  }

  private RecommendationUploadCSVRowData parseCSVRow(CSVRecord record) {
    String recordCustomerNumber = record.get("CUSTOMER_NUMBER");
    String recordRecommendationActive = record.get("RECOMMENDATION_ACTIVE");
    final List<String> recommendations = new ArrayList<>();
    for (int i = 1; i < 11; i++) {
      String recordRecommendation = record.get("REC" + i);
      if (! StringUtils.isEmpty(recommendations)) {
        recommendations.add(recordRecommendation);
      }
    }
    final long customerNumer = NumberUtils.parseNumber(recordCustomerNumber, Long.class);
    final boolean isRecommendationActive = ! StringUtils.isEmpty(recordRecommendationActive) && "true"
        .equalsIgnoreCase(recordRecommendationActive);
    RecommendationUploadCSVRowData entry = new RecommendationUploadCSVRowData(customerNumer,
                                                                              isRecommendationActive,
                                                                              recommendations);
    return entry;
  }

}
