package de.esailors.gamerecommendation.service;

import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.model.persistence.RecommendationEntry;
import de.esailors.gamerecommendation.repositories.CustomerRepository;
import de.esailors.gamerecommendation.service.exceptions.RecommendationUploadServiceException;
import de.esailors.gamerecommendation.tools.RecommendationUploadCSVParser;
import de.esailors.gamerecommendation.tools.RecommendationUploadCSVRowData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of a {@link RecommendationUploadService} which stores the data into our managed database.
 */
@Service
public class DatabaseStoredRecommendationUploadService implements RecommendationUploadService {

  private final CustomerRepository customerRepository;

  @Autowired
  public DatabaseStoredRecommendationUploadService(CustomerRepository pCustomerRepository) {
    this.customerRepository = pCustomerRepository;
  }

  @Override public void store(MultipartFile pFile) throws RecommendationUploadServiceException {
    List<RecommendationUploadCSVRowData> entries = parseEntries(pFile);
    insertEntries(entries);
  }

  private List<RecommendationUploadCSVRowData> parseEntries(MultipartFile pFile)
      throws RecommendationUploadServiceException {
    try {
      if (pFile.isEmpty()) {
        throw new RecommendationUploadServiceException("File is empty");
      }
      if (! pFile.getOriginalFilename().toUpperCase().endsWith(".CSV")) {
        throw new RecommendationUploadServiceException("Filetype must be .CSV ");
      }

      List<RecommendationUploadCSVRowData> parsedData = new RecommendationUploadCSVParser()
          .parse(pFile.getInputStream());
      return parsedData;
    } catch (IOException e) {
      throw new RecommendationUploadServiceException("Failed to parse file " + pFile.getOriginalFilename() + ": " + e
          .getLocalizedMessage());
    }
  }

  private void insertEntries(List<RecommendationUploadCSVRowData> entries)
      throws RecommendationUploadServiceException {
    for (RecommendationUploadCSVRowData entry : entries) {
      try {
        Optional<Customer> customer = customerRepository.findById(entry.getCustomerNumber());
        //we should think on multithreading here, so that not same customer is modified at same time.
        if (customer.isPresent()) {
          modifyCustomer(entry, customer);
        } else {
          createNewCustomer(entry);
        }
      } catch (Exception ex) {
        throw new RecommendationUploadServiceException("Error importing processed dataset : " + ex.getLocalizedMessage
            ());
      }
    }
  }

  /**
   * create a new customer entity with given initial data
   *
   * @param pEntry initial data POJO
   */
  private void createNewCustomer(RecommendationUploadCSVRowData pEntry) {
    Customer newCustomer = new Customer(pEntry.getCustomerNumber());
    newCustomer.setRecommendationActive(pEntry.isRecommendationsActive());
    for (String recommendation : pEntry.getRecommendations()) {
      newCustomer.addRecommendations(new RecommendationEntry(recommendation));
    }
    customerRepository.save(newCustomer);
  }

  /**
   * updates an already existing customer with given data
   *
   * @param pEntry    new data POJO
   * @param pCustomer existing entity
   */
  private void modifyCustomer(RecommendationUploadCSVRowData pEntry, Optional<Customer> pCustomer) {
    Customer customerToBeUpdated = pCustomer.get();
    customerToBeUpdated.setRecommendationActive(pEntry.isRecommendationsActive());
    //we do not merge the recommendations here, so old entries are removed, new inserted
    customerToBeUpdated.getRecommendationEntries().clear();
    for (String recommendation : pEntry.getRecommendations()) {
      customerToBeUpdated.addRecommendations(new RecommendationEntry(recommendation));
    }
    customerRepository.save(customerToBeUpdated);
  }

}
