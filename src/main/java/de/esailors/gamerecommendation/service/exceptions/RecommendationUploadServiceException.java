package de.esailors.gamerecommendation.service.exceptions;

/**
 * Exception which can be thrown during parsing and processing the Upload of new data-sets for recommendations.
 */
public class RecommendationUploadServiceException extends Exception {

  /**
   * New exception instance with given error message
   *
   * @param pMessage error message
   */
  public RecommendationUploadServiceException(String pMessage) {
    super(pMessage);
  }

}
