package de.esailors.gamerecommendation.service;

import de.esailors.gamerecommendation.service.exceptions.RecommendationUploadServiceException;
import org.springframework.web.multipart.MultipartFile;

/**
 * API for processing of csv-datasets from uploaded file
 */
public interface RecommendationUploadService {

  /**
   * Stores the given file dataset in local database.
   *
   * @param pFile uploaded File
   * @throws RecommendationUploadServiceException error during processing and parsing file
   */
  public void store(MultipartFile pFile) throws RecommendationUploadServiceException;
}
