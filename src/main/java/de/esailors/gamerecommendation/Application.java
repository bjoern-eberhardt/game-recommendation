package de.esailors.gamerecommendation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Entrypoint class for SpringBoot
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableAutoConfiguration
@EntityScan("de.esailors.gamerecommendation.model")
@EnableJpaRepositories("de.esailors.gamerecommendation.repositories")
public class Application {

  /**
   * Can be used for starting the Application via IDE
   *
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
