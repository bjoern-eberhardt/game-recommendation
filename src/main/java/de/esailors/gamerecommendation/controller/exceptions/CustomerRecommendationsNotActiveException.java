package de.esailors.gamerecommendation.controller.exceptions;

/**
 * Exception if customer recommendations are not activated
 */
public class CustomerRecommendationsNotActiveException extends RuntimeException {

  /**
   * New instance
   */
  public CustomerRecommendationsNotActiveException() {
    super("Customer recommendations not activated");
  }

  /**
   * New error instance with given message
   *
   * @param pMessage message
   */
  public CustomerRecommendationsNotActiveException(String pMessage) {
    super(pMessage);
  }

}
