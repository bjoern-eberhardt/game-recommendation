package de.esailors.gamerecommendation.controller.exceptions;

/**
 * Exception if customer is not found
 */
public class CustomerNotFoundException extends RuntimeException {

  /**
   * New instance
   */
  public CustomerNotFoundException() {
    super("Customer not found");
  }

  /**
   * New instance of error with given message
   *
   * @param pMessage message
   */
  public CustomerNotFoundException(String pMessage) {
    super(pMessage);
  }

}
