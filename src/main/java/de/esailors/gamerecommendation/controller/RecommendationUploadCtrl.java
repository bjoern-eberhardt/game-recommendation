package de.esailors.gamerecommendation.controller;

import de.esailors.gamerecommendation.service.RecommendationUploadService;
import de.esailors.gamerecommendation.service.exceptions.RecommendationUploadServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for Uploading mime-multitype csv files for data import.
 */
@Controller
public class RecommendationUploadCtrl {

  private final RecommendationUploadService recommendationUploadService;

  @Autowired
  public RecommendationUploadCtrl(RecommendationUploadService pRecommendationUploadService) {
    this.recommendationUploadService = pRecommendationUploadService;
  }

  @PostMapping("/upload")
  public ResponseEntity<?> handleFileUpload(@RequestParam(value = "file", required = false) MultipartFile file) throws
      RecommendationUploadServiceException {
    //if parameter not set, return status code = 204
    if (file == null) {
      return ResponseEntity.noContent().build();
    }
    //try to parse this file
    recommendationUploadService.store(file);
    return ResponseEntity.ok().build();
  }

  @ExceptionHandler(RecommendationUploadServiceException.class)
  public ResponseEntity<?> handleStorageFileNotFound(RecommendationUploadServiceException exc) {
    return ResponseEntity.unprocessableEntity().body(exc.getLocalizedMessage());
  }
}
