package de.esailors.gamerecommendation.controller;

import de.esailors.gamerecommendation.controller.exceptions.CustomerNotFoundException;
import de.esailors.gamerecommendation.controller.exceptions.CustomerRecommendationsNotActiveException;
import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.model.persistence.RecommendationEntry;
import de.esailors.gamerecommendation.model.view.RecommendationDTO;
import de.esailors.gamerecommendation.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller that returns the recommendations of given customer
 */
@RestController
@RequestMapping("/customers/{customerId:\\d+}/games/recommendations")
public class RecommendationCtrl {

  private CustomerRepository repository;

  @Autowired
  public RecommendationCtrl(CustomerRepository pRepository) {
    this.repository = pRepository;
  }

  /**
   * Processes the GET request on given {@link RequestMapping} of class.
   *
   * @param pCustomerId CustomerId is parsed from URL and must be a number
   * @param pCount      Parameter of request named "count" is optional and defaults to 5. The parameter gives max length
   *                    of recommendations. If this value is greater than available recommendations, all available
   *                    recommendations will be returned
   * @return JSON serialized result
   */
  @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public RecommendationDTO getRecommendations(@PathVariable("customerId") Long pCustomerId,
      @RequestParam(value = "count", defaultValue = "5") String pCount) {

    final int count = parseInteger(pCount, 5);
    Optional<Customer> foundCustomer = repository.findById(pCustomerId);
    if (! foundCustomer.isPresent()) {
      throw new CustomerNotFoundException();
    }

    Customer customer = foundCustomer.get();
    if (! customer.isRecommendationActive()) {
      throw new CustomerRecommendationsNotActiveException();
    }

    RecommendationDTO model = transformDataForView(count, customer);
    return model;
  }

  @ExceptionHandler(CustomerNotFoundException.class)
  public ResponseEntity<?> handleCustomerNotFoundException(CustomerNotFoundException exc) {
    return ResponseEntity.notFound().build();
  }

  @ExceptionHandler(CustomerRecommendationsNotActiveException.class)
  public ResponseEntity<?> handleCustomerRecommendationsNotActiveException(
      CustomerRecommendationsNotActiveException exc) {
    return ResponseEntity.notFound().build();
  }

  /**
   * Transformation from entity model to our transfer model for view
   *
   * @param pCount    number of recommendations that are requestet (max)
   * @param pCustomer customer entity, that should be transformed into DTO
   * @return DTO for view
   */
  private RecommendationDTO transformDataForView(int pCount, Customer pCustomer) {
    //create copy so that persistence list is not modified and shuffle
    List<RecommendationEntry> recommendations = new ArrayList(pCustomer.getRecommendationEntries());
    Collections.shuffle(recommendations);

    List<String> finalRecommendationList = recommendations.stream().map
        (RecommendationEntry::getRecommendation).limit(pCount)
                                                          .collect
                                                              (Collectors.toList());
    return new RecommendationDTO(pCustomer.getCustomerNumber(), finalRecommendationList);
  }

  private int parseInteger(String pValue, int pDefaultValue) {
    try {
      return Integer.valueOf(pValue);
    } catch (NumberFormatException e) {
      return pDefaultValue;
    }
  }

}
