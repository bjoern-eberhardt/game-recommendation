package de.esailors.gamerecommendation.testutils;

import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.model.persistence.RecommendationEntry;
import org.assertj.core.api.Assertions;

/**
 * Test tools class, that specially asserts the customer object
 */
public class CustomerAssertions {

  private CustomerAssertions() {
  }

  ;

  /**
   * asserts the given customer with exptected data
   *
   * @param pCustomer                      customer to be asserted
   * @param pExpectedRecommendationActive  should the recommendation be active for given customer?
   * @param pExpectedCustomerId            exptected customer number of given customer
   * @param pExpectedRecommendationEntries expected list of recommendations for given customer.
   */
  public static void assertsCustomer(Customer pCustomer, boolean pExpectedRecommendationActive, long
      pExpectedCustomerId, String... pExpectedRecommendationEntries) {

    Assertions.assertThat(pCustomer.isRecommendationActive()).isEqualTo(pExpectedRecommendationActive);
    Assertions.assertThat(pCustomer.getCustomerNumber()).isEqualTo(pExpectedCustomerId);
    Assertions.assertThat(pCustomer.getRecommendationEntries()).hasSize(pExpectedRecommendationEntries.length);
    Assertions.assertThat(pCustomer.getRecommendationEntries()
                                   .stream()
                                   .map(RecommendationEntry::getRecommendation))
              .containsExactlyInAnyOrder(pExpectedRecommendationEntries);
  }
}
