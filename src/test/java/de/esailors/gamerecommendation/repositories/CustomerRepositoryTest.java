package de.esailors.gamerecommendation.repositories;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.model.persistence.RecommendationEntry;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Optional;

import static de.esailors.gamerecommendation.testutils.CustomerAssertions.assertsCustomer;

/**
 * Test for {@link CustomerRepository} which is linked to persistence layer to query and process {@link Customer}
 * entitiy.<br>We use SpringTestDB at this point to run database tests for initial import.
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
//we use database testdata from classpath, which is inserted before each unit test method to have same dataset
@DatabaseSetup(CustomerRepositoryTest.TEST_DATA)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CustomerRepositoryTest.TEST_DATA })
public class CustomerRepositoryTest {

  protected static final String TEST_DATA = "classpath:testdata/customer-data.xml";

  @Autowired
  private CustomerRepository repository;

  /**
   * There are three customers available in test data
   */
  @Test
  public void testFindAllCustomers() {
    Iterable<Customer> data = repository.findAll();
    Assertions.assertThat(data).hasSize(3);
  }

  /**
   * Customer 1 should be found. Its id should match and its two recommendations, too.
   */
  @Test
  public void testFindCustomerById_1() {
    Optional<Customer> item1 = repository.findById(Long.valueOf(1));
    Assertions.assertThat(item1).isPresent();
    assertsCustomer(item1.get(), true, 1, "ping", "solitaire");
  }

  /**
   * Customer 2 should be found. Its id should match and its three recommendations, too. The recommendations are
   * inactive.
   */
  @Test
  public void testFindCustomerById_2() {
    Optional<Customer> item2 = repository.findById(Long.valueOf(2));
    Assertions.assertThat(item2).isPresent();
    assertsCustomer(item2.get(), false, 2, "uno", "ping", "solitaire");
  }

  /**
   * Customer 3 should be found. Its id should match and its two recommendations, too.
   */
  @Test
  public void testFindCustomerById_3() {
    Optional<Customer> item3 = repository.findById(Long.valueOf(3));
    Assertions.assertThat(item3).isPresent();
    assertsCustomer(item3.get(), true, 3, "kniffel");
  }

  /**
   * We search for an customer, that does not exists
   */
  @Test
  public void testFindCustomerById_NotFound() {
    Optional<Customer> item3 = repository.findById(Long.MAX_VALUE);
    Assertions.assertThat(item3).isNotPresent();
  }

  /**
   * We search for an customer, that does not exists
   */
  @Test
  public void testExistsById_NotFound() {
    Assertions.assertThat(repository.existsById(Long.MAX_VALUE)).isFalse();
  }

  /**
   * We search for an customer, that  exists in test data
   */
  @Test
  public void testExistsById_Found() {
    Assertions.assertThat(repository.existsById(Long.valueOf(1))).isTrue();
  }

  /**
   * We insert a new customer with two recommendations. Afterwards, this customer should be queryable and total size
   * has increased by 1.
   */
  @Test
  public void testInsertNewCustomer() {
    long sizeBefore = repository.count();
    Customer c = new Customer((long) 42);
    c.addRecommendations(new RecommendationEntry("rec1"));
    c.addRecommendations(new RecommendationEntry("rec2"));
    Customer result = repository.save(c);

    Assertions.assertThat(result.getCustomerNumber()).isEqualTo(42);
    Assertions.assertThat(result.getRecommendationEntries()).hasSize(2);
    Assertions.assertThat(repository.findAll()).hasSize((int) sizeBefore + 1);
  }

  /**
   * We get a customer and add a new recommendation entry. It should be there afterwards but updated, no new customer!.
   */
  @Test
  public void testUpdatingCustomer() {
    long sizeBefore = repository.count();
    Optional<Customer> c = repository.findById((long) 1);
    Assertions.assertThat(c).isPresent();
    Customer customerInEditMode = c.get();
    customerInEditMode.setRecommendationActive(false);
    customerInEditMode.addRecommendations(new RecommendationEntry("rec1"));
    Customer result = repository.save(customerInEditMode);
    assertsCustomer(result, false, 1, "ping", "solitaire", "rec1");

    Assertions.assertThat(repository.findAll()).hasSize((int) sizeBefore);
  }

  /**
   * We get a customer and remove one recommendation entry. It should be removed afterwards
   */
  @Test
  public void testUpdatingCustomer_RemoveRecommendation() {
    long sizeBefore = repository.count();
    Optional<Customer> c = repository.findById((long) 1);
    Assertions.assertThat(c).isPresent();
    Customer customerInEditMode = c.get();
    Assertions.assertThat(customerInEditMode.getRecommendationEntries().removeIf(e -> "ping".equals(e.getRecommendation
        ()))).isTrue();

    Customer result = repository.save(customerInEditMode);
    assertsCustomer(result, true, 1, "solitaire");

    Assertions.assertThat(repository.findAll()).hasSize((int) sizeBefore);
  }

  /**
   * We remove an existing customer. It is not available anymore and total size has decreased.
   */
  @Test
  public void testDeletingCustomer() {
    long sizeBefore = repository.count();
    Optional<Customer> c = repository.findById((long) 1);
    Assertions.assertThat(c).isPresent();
    Customer customerInEditMode = c.get();
    repository.delete(customerInEditMode);
    Assertions.assertThat(repository.findAll()).hasSize((int) sizeBefore - 1);
    Assertions.assertThat(repository.existsById((long) 1)).isFalse();
  }

}