package de.esailors.gamerecommendation.controller;

import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.repositories.CustomerRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Optional;

import static de.esailors.gamerecommendation.testutils.CustomerAssertions.assertsCustomer;

/**
 * Test for {@link RecommendationUploadCtrl}, which processes uploaded files as dataset to be inserted into
 * persistence layer.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecommendationUploadCtrlTest {

  protected static final String UPLOAD_PATH = "/upload";

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CustomerRepository repository;

  /**
   * We upload test csv file with two dataasets.
   *
   * @throws Exception
   */
  @Test
  public void testUploadFile() throws Exception {
    long countBefore = repository.count();
    MultiValueMap<String, Object> param = createFileUploadParams("testdata/correctUpload.csv", "file");
    ResponseEntity<String> response = this.restTemplate.postForEntity(RecommendationUploadCtrlTest.UPLOAD_PATH,
                                                                      param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
    //we inserted two more data ...
    Assertions.assertThat(repository.count()).isEqualTo(countBefore + 2);

    Assertions.assertThat(repository.existsById((long) 111111)).isTrue();
    Assertions.assertThat(repository.existsById((long) 111112)).isTrue();

    Optional<Customer> customer11111 = repository.findById((long) 111111);
    Assertions.assertThat(customer11111).isPresent();
    assertsCustomer(customer11111.get(), true, 111111, "bingo", "cashwheel", "cashbuster", "brilliant", "citytrio",
                    "crossword", "sevenwins", "sudoku", "sofortlotto", "hattrick");

    Optional<Customer> customer11112 = repository.findById((long) 111112);
    Assertions.assertThat(customer11112).isPresent();
    assertsCustomer(customer11112.get(), false, 111112, "brilliant", "citytrio", "crossword", "sevenwins", "sudoku",
                    "sofortlotto", "hattrick", "bingo", "cashwheel", "cashbuster");
  }

  /**
   * We upload test dat file and expect error, because filename does not end with "CSV".
   *
   * @throws Exception
   */
  @Test
  public void testUploadFileWithWrongFileformat() throws Exception {
    MultiValueMap<String, Object> param = createFileUploadParams
        ("testdata/wrongUpload.dat", "file");
    ResponseEntity<String> response = this.restTemplate.postForEntity(UPLOAD_PATH, param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    Assertions.assertThat(response.getBody()).contains("Filetype must be .CSV");
  }

  /**
   * We upload csv without HEADER line and expect an error
   *
   * @throws Exception
   */
  @Test
  public void testUploadFileWithMissingHeader() throws Exception {
    MultiValueMap<String, Object> param = createFileUploadParams
        ("testdata/headerMissingUpload.csv", "file");
    ResponseEntity<String> response = this.restTemplate.postForEntity(RecommendationUploadCtrlTest.UPLOAD_PATH, param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    Assertions.assertThat(response.getBody()).contains("Error parsing data. Uncorrect format");
  }

  /**
   * We upload dataset where a customer number is NaN.
   *
   * @throws Exception
   */
  @Test
  public void testUploadFileWithWrongFileformatInRow() throws Exception {
    MultiValueMap<String, Object> param = createFileUploadParams
        ("testdata/illegalDataUpload.csv", "file");
    ResponseEntity<String> response = this.restTemplate.postForEntity(RecommendationUploadCtrlTest.UPLOAD_PATH, param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    Assertions.assertThat(response.getBody()).contains("Error parsing data. Uncorrect format of customer-number.");
  }

  /**
   * We upload an empty file. Error is expected
   *
   * @throws Exception
   */
  @Test
  public void testUploadEmptyFile() throws Exception {
    MultiValueMap<String, Object> param = createFileUploadParams
        ("testdata/emptyFile.csv", "file");
    ResponseEntity<String> response = this.restTemplate.postForEntity(RecommendationUploadCtrlTest.UPLOAD_PATH, param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    Assertions.assertThat(response.getBody()).contains("File is empty");
  }

  /**
   * We upload an file but used wrong parameterName. Error is expteced
   *
   * @throws Exception
   */
  @Test
  public void testUploadFileWithWrongParam() throws Exception {
    MultiValueMap<String, Object> param = createFileUploadParams("testdata/emptyFile.csv", "someThing");
    ResponseEntity<String> response = this.restTemplate.postForEntity(RecommendationUploadCtrlTest.UPLOAD_PATH, param,
                                                                      String.class);

    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

  /**
   * Creates a {@link MultiValueMap} where the File is added as given parameter to request later
   * @param pFileClassPath Filepath to file to be "uploaded" (from classpath location)
   * @param pFileParameterName name of parameter to be used for filedata
   * @return map to be used in request
   */
  private MultiValueMap<String, Object> createFileUploadParams(String pFileClassPath, String pFileParameterName) {
    ClassPathResource resource = new ClassPathResource(pFileClassPath);

    MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
    map.add(pFileParameterName, resource);
    return map;
  }

}