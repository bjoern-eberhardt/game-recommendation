package de.esailors.gamerecommendation.controller;

import de.esailors.gamerecommendation.model.persistence.Customer;
import de.esailors.gamerecommendation.model.persistence.RecommendationEntry;
import de.esailors.gamerecommendation.repositories.CustomerRepository;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static net.minidev.json.parser.JSONParser.MODE_JSON_SIMPLE;

/**
 * Test for {@link RecommendationCtrl} which returns recommendations for given customerNumber
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecommendationCtrlTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CustomerRepository repository;

  @Before
  public void setupTestdata() {
    Customer c = new Customer((long) 1111);
    c.setRecommendationActive(true);
    c.addRecommendations(new RecommendationEntry("pong"));
    c.addRecommendations(new RecommendationEntry("ping"));
    c.addRecommendations(new RecommendationEntry("solitaire"));
    c.addRecommendations(new RecommendationEntry("uno"));
    c.addRecommendations(new RecommendationEntry("citytrio"));
    c.addRecommendations(new RecommendationEntry("crossword"));
    repository.save(c);

    c = new Customer((long) 1112);
    c.setRecommendationActive(false);
    c.addRecommendations(new RecommendationEntry("pong"));
    repository.save(c);
  }

  /**
   * Without count parameter the default of "5" recommendations will be returned
   */
  @Test
  public void testGetRecommendationsWithoutCountParam() {
    ResponseEntity<String> response = this.restTemplate
        .getForEntity("/customers/1111/games/recommendations", String.class);
    Assertions.assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    String body = response.getBody();
    assertJSONResult(body, 1111, 5, "pong", "ping", "solitaire", "uno", "citytrio", "crossword");
  }

  /**
   * Count parameter is processed, only one recommendation is expected
   */
  @Test
  public void testGetRecommendationsWithCountParam() {
    ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1111/games/recommendations?count=1",
                                                                     String.class);
    Assertions.assertThat(response.getHeaders().getContentType()).as(response.getBody()).isEqualTo(MediaType
                                                                                                       .APPLICATION_JSON_UTF8);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    String body = response.getBody();
    assertJSONResult(body, 1111, 1, "pong", "ping", "solitaire", "uno", "citytrio", "crossword");
  }

  /**
   * Tests whether recommendations will be shuffled.
   */
  @Test
  public void testGetRecommendationsShuffle() {
    int numberOfRuns = 10;
    Set<String> resultingRecommendations = new HashSet<>();
    //we run the request a few times and will get always one result. Because of shuffling, there must be more than 1
    // result in whole dataset for all requests
    for (int i = 0; i < numberOfRuns; i++) {
      ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1111/games/recommendations?count=1",
                                                                       String.class);
      Assertions.assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);
      Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

      String body = response.getBody();
      Set<String> queriedRecommendations = getRecommendationsFromJSONResult(body);
      Assertions.assertThat(queriedRecommendations).hasSize(1);
      resultingRecommendations.addAll(queriedRecommendations);
    }
    Assertions.assertThat(resultingRecommendations.size()).isGreaterThan(1);
  }

  /**
   * if count parameter is greater than available recommendations, just return the max possible recommendations
   */
  @Test
  public void testGetRecommendationsWithtCountParamGreaterThanData() {
    ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1111/games/recommendations?count=100",
                                                                     String.class);
    Assertions.assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    String body = response.getBody();
    assertJSONResult(body, 1111, 6, "pong", "ping", "solitaire", "uno", "citytrio", "crossword");
  }

  /**
   * Wrong customerNumber will get a 404
   */
  @Test
  public void testCustomerNotActive() {
    ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1112/games/recommendations?count=10",
                                                                     String.class);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  /**
   * CustomerNumber which does not exists will get a 404
   */
  @Test
  public void testCustomerNotFound() {
    ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1/games/recommendations?count=1",
                                                                     String.class);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  /**
   * if customerNumber is not a nuber, expect a 404 error
   */
  @Test
  public void testCustomerNaN() {
    ResponseEntity<String> response = this.restTemplate.getForEntity("/customers/1a/games/recommendations?count=1",
                                                                     String.class);
    Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  /**
   * asserts the JSON result of request
   *
   * @param pJSONResult                 JSON as String
   * @param pExpectedCustomerNumber     expected customer number
   * @param pExpectedRecommendationSize expected size of recommendation entries
   * @param pListOfValidValues          set of valid recommendations. if length of pListOfValidValues is same as
   *                                    pExpectedRecommendationSize all valid recommendations must be used, otherwise a subset
   *                                    with expected recommendation size
   */
  private void assertJSONResult(String pJSONResult, long pExpectedCustomerNumber, int pExpectedRecommendationSize,
      String... pListOfValidValues) {
    JSONParser parser = new JSONParser(MODE_JSON_SIMPLE);
    try {
      JSONObject parsedJSON = (JSONObject) parser.parse(pJSONResult);
      Assertions.assertThat(parsedJSON.getAsNumber("customerId")).isEqualTo(pExpectedCustomerNumber);
      Assertions.assertThat(parsedJSON.get("recommendations")).isInstanceOf(JSONArray.class);
      JSONArray recommendations = (JSONArray) parsedJSON.get("recommendations");
      Assertions.assertThat(recommendations.size()).isEqualTo(pExpectedRecommendationSize);
      if (pListOfValidValues != null) {
        ArrayList<String> list = Lists.newArrayList(pListOfValidValues);
        if (pExpectedRecommendationSize != pListOfValidValues.length) {
          Assertions.assertThat(recommendations).isSubsetOf(list);
        } else {
          Assertions.assertThat(recommendations).containsOnlyElementsOf(list);
        }
      }
    } catch (ParseException e) {
      Assertions.fail("Result is not well-json-formatted: " + pJSONResult);
    }
  }

  private Set<String> getRecommendationsFromJSONResult(String pJSONResult) {
    Set<String> result = new HashSet<>();
    JSONParser parser = new JSONParser(MODE_JSON_SIMPLE);
    try {
      JSONObject parsedJSON = (JSONObject) parser.parse(pJSONResult);
      Assertions.assertThat(parsedJSON.get("recommendations")).isInstanceOf(JSONArray.class);
      JSONArray recommendations = (JSONArray) parsedJSON.get("recommendations");
      Iterator<Object> it = recommendations.iterator();
      while (it.hasNext()) {
        String recommendationEntry = (String) it.next();
        result.add(recommendationEntry);
      }
    } catch (ParseException e) {
      Assertions.fail("Result is not well-json-formatted: " + pJSONResult);
    }
    return result;
  }

}